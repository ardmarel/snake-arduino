#include <RGBmatrixPanel.h>
#include <string.h>
#include <Wire.h>

// Most of the signal pins are configurable, but the CLK pin has some
// special constraints.  On 8-bit AVR boards it must be on PORTB...
// Pin 8 works on the Arduino Uno & compatibles (e.g. Adafruit Metro),
// Pin 11 works on the Arduino Mega.  On 32-bit SAMD boards it must be
// on the same PORT as the RGB data pins (D2-D7)...
// Pin 8 works on the Adafruit Metro M0 or Arduino Zero,
// Pin A4 works on the Adafruit Metro M4 (if using the Adafruit RGB
// Matrix Shield, cut trace between CLK pads and run a wire to A4).

//#define CLK  8   // USE THIS ON ARDUINO UNO, ADAFRUIT METRO M0, etc.
//#define CLK A4 // USE THIS ON METRO M4 (not M0)
#define CLK 11 // USE THIS ON ARDUINO MEGA
#define OE   9
#define LAT 10
#define A   A0
#define B   A1
#define C   A2

#define SCREEN_WIDTH 31
#define SCREEN_HEIGHT 15
#define MAX_SNAKE_LENGTH 512
#define FPS 9
#define I2C_EFFECT_ID 10

static uint8_t _a[] = {0, 1, 0,
                      1, 0, 1,
                      1, 1, 1,
                      1, 0, 1};

static uint8_t _b[] = {1, 0, 0,
                      1, 1, 0,
                      1, 0, 1,
                      1, 1, 0};

static uint8_t _c[] = {0, 1, 1,
                      1, 0, 0,
                      1, 0, 0,
                      0, 1, 1};

static uint8_t _d[] = {0, 0, 1,
                      0, 1, 1,
                      1, 0, 1,
                      0, 1, 1};

static uint8_t _e[] = {1, 1, 1,
                      1, 1, 1,
                      1, 0, 0,
                      1, 1, 1};

static uint8_t _f[] = {1, 1,
                      1, 0,
                      1, 1,
                      1, 0};

static uint8_t _g[] = {1, 1, 1,
                      1, 1, 1,
                      0, 0, 1,
                      0, 1, 1};

static uint8_t _h[] = {1, 0, 0,
                      1, 0, 0,
                      1, 1, 1,
                      1, 0, 1};

static uint8_t _i[] = {1,
                      0,
                      1,
                      1};

static uint8_t _j[] = {0, 1,
                      0, 0,
                      0, 1,
                      1, 1};

static uint8_t _k[] = {1, 0, 1,
                      1, 0, 1,
                      1, 1, 0,
                      1, 0, 1};

static uint8_t _l[] = {1, 0,
                      1, 0,
                      1, 0,
                      1, 1};

static uint8_t _m[] = {0, 1, 0, 1, 0,
                      1, 0, 1, 0, 1,
                      1, 0, 1, 0, 1,
                      1, 0, 1, 0, 1};

static uint8_t _n[] = {0, 1, 0,
                      1, 0, 1,
                      1, 0, 1,
                      1, 0, 1};

static uint8_t _o[] = {0, 1, 0,
                      1, 0, 1,
                      1, 0, 1,
                      0, 1, 0};

static uint8_t _p[] = {1, 1, 1,
                      1, 0, 1,
                      1, 1, 1,
                      1, 0, 0};

static uint8_t _q[] = {1, 1, 1,
                      1, 0, 1,
                      1, 1, 1,
                      0, 0, 1};

static uint8_t _r[] = {1, 1, 0,
                      1, 0, 1,
                      1, 1, 0,
                      1, 0, 1};

static uint8_t _s[] = {1, 1,
                      1, 0,
                      0, 1,
                      1, 1};

static uint8_t _t[] = {1, 1, 1,
                      0, 1, 0,
                      0, 1, 0,
                      0, 1, 0};

static uint8_t _u[] = {1, 0, 1,
                      1, 0, 1,
                      1, 0, 1,
                      1, 1, 1};

static uint8_t _v[] = {1, 0, 1,
                      1, 0, 1,
                      1, 0, 1,
                      0, 1, 0};

static uint8_t _w[] = {1, 0, 1, 0, 1,
                      1, 0, 1, 0, 1,
                      1, 0, 1, 0, 1,
                      0, 1, 0, 1, 0};

static uint8_t _x[] = {1, 0, 1,
                      0, 1, 0,
                      0, 1, 0,
                      1, 0, 1};

static uint8_t _y[] = {1, 0, 1,
                      0, 1, 0,
                      0, 1, 0,
                      0, 1, 0};

static uint8_t _z[] = {1, 1,
                      0, 1,
                      1, 0,
                      1, 1};

static uint8_t _1[] = {0, 1, 0,
                      1, 1, 0,
                      0, 1, 0,
                      1, 1, 1};

static uint8_t _2[] = {0, 1, 1,
                      1, 0, 1,
                      0, 1, 0,
                      1, 1, 1};

static uint8_t _3[] = {1, 1, 1,
                      0, 1, 1,
                      0, 0, 1,
                      1, 1, 1};

static uint8_t _4[] = {1, 0, 1,
                      1, 1, 1,
                      0, 0, 1,
                      0, 0, 1};

static uint8_t _6[] = {1, 1,
                      1, 0,
                      1, 1,
                      1, 1};

static uint8_t _7[] = {1, 1,
                      0, 1,
                      0, 1,
                      0, 1};

static uint8_t _8[] = {1, 0, 1,
                      1, 1, 1,
                      1, 0, 1,
                      1, 1, 1};

static uint8_t _9[] = {1, 1, 1,
                      1, 1, 1,
                      0, 0, 1,
                      0, 0, 1};

static uint8_t _empty[] = {0,
                         0,
                         0,
                         0};

uint16_t color_red;
uint16_t color_green;
uint16_t color_blank;

unsigned long lastFrameTime;
const unsigned long timeBetweenFrames = 1000 / FPS;
char snakeX[MAX_SNAKE_LENGTH];
char snakeY[MAX_SNAKE_LENGTH];
unsigned short lenIndex;
unsigned short topScores[3];
char topScoreNames[3][3];
short appleX;
short appleY;
char newInput;
char direction = 2; //0=up, 1=left, 2=right, 3=bottom

RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false);

void setup() {
  pinMode(22, INPUT);
  pinMode(23, INPUT);
  pinMode(24, INPUT);
  pinMode(25, INPUT);
  matrix.begin();
  Wire.begin();

  color_red = matrix.Color333(2, 0, 0);
  color_green = matrix.Color333(0, 2, 0);
  color_blank = matrix.Color333(0, 0, 0);

  startGame();
}

void loop() {
  getDirection();

  if(millis() > lastFrameTime + timeBetweenFrames){
    char lastX = snakeX[lenIndex];
    char lastY = snakeY[lenIndex];
    direction = newInput;
    
    //After eating the apple the length of the snake increases
    if (snakeX[0] != appleX || snakeY[0] != appleY){
      // Last pixel of the snake disappears
      matrix.drawPixel(lastX, snakeY[lenIndex], color_blank);
    }
    else if (lenIndex < MAX_SNAKE_LENGTH-1){
      ++lenIndex;
      placeApple();
    }

    // When the length of the snake is 1 there isnt a lot to move. index 0 is head and lenIndex is tail
    if (lenIndex > 0){
      for(short i=lenIndex-1; i>=0; --i){
        snakeX[i+1] = snakeX[i];
        snakeY[i+1] = snakeY[i];
      }
    }
    else{
      snakeX[1] = snakeX[0];
      snakeY[1] = snakeY[0];
    }

    // Draw the pixel where the snake will move into. for the terminary the normal statement is for normal movement, the other is for teleportation
    switch(direction){
      case 0:
        snakeX[0] = snakeX[1];
        snakeY[0] = snakeY[0] > 0 ? snakeY[1]-1 : SCREEN_HEIGHT;
        break;
      case 1:
        snakeY[0] = snakeY[1];
        snakeX[0] = snakeX[0] > 0 ? snakeX[1]-1 : SCREEN_WIDTH;
        break;
      case 2:
        snakeY[0] = snakeY[1];
        snakeX[0] = snakeX[0] < SCREEN_WIDTH ? snakeX[1]+1 : 0;
        break;
      case 3:
        snakeX[0] = snakeX[1];
        snakeY[0] = snakeY[0] < SCREEN_HEIGHT ? snakeY[1]+1 : 0;
        break;
    }

    // Check if the snake eats itself
    char currentX = snakeX[0];
    char currentY = snakeY[0];

    bool dead = false;
    
    int i=3;
    
    while(i<MAX_SNAKE_LENGTH-1) {
      if(currentX == snakeX[i] && currentY == snakeY[i]){
        dead = true;
        matrix.drawPixel(lastX, lastY, color_green);  
        playEffect(2);
        delay(4000);
        showScore();
        newTopScore();
        showTopScores();
        startGame();
        break;
      }
      ++i;
    }

    if(!dead){
      matrix.drawPixel(snakeX[0], snakeY[0], color_green);
      lastFrameTime = millis();
    }
    dead = false;
  }
}

void startGame(){
  matrix.fillScreen(color_blank);

  newInput = 4;
  
  // prepare snake array for game
  for (short i=0; i<MAX_SNAKE_LENGTH-1; ++i){
    snakeX[i] = 127;
    snakeY[i] = 127;
  }

  // start position
  snakeX[0]=8;
  snakeY[0]=8;
  lenIndex = 0;

  placeApple();
}

// generates apple
void placeApple(){
  playEffect(1);
  
  bool emptyPixelFound = false;
  while (!emptyPixelFound){
    emptyPixelFound = true;
    appleX = (short) random(SCREEN_WIDTH);
    appleY = (short) random(SCREEN_HEIGHT);

    for(int i=0; i<=lenIndex; ++i) {
      if(appleX == snakeX[i] && appleY == snakeY[i]){
        emptyPixelFound = false; 
      }
    }
  }

  matrix.drawPixel(appleX, appleY, color_red);
}

void showScore(){
  matrix.fillScreen(color_blank);

  if (lenIndex + 1 >= topScores[2]){
    playEffect(3);  
  }

  String score = String(lenIndex+1);

  short letterX = 4;
  for (int letterIndex=0; letterIndex<score.length(); ++letterIndex){
      matrix.drawChar(letterX, 4, score[letterIndex], color_green, color_blank, 1);
      letterX += 6;
  }

  waitForButtonInMenu();
}

void newTopScore(){
  matrix.fillScreen(color_blank);
  newInput = 4;
  short score = lenIndex+1;
  char index = 3;
  if (score >= topScores[0]){
    index = 0;
    topScores[2] = topScores[1];

    topScoreNames[2][0] = topScoreNames[1][0];
    topScoreNames[2][1] = topScoreNames[1][1];
    topScoreNames[2][2] = topScoreNames[1][1];
    
    topScores[1] = topScores[0];

    topScoreNames[1][0] = topScoreNames[0][0];
    topScoreNames[1][1] = topScoreNames[0][1];
    topScoreNames[1][2] = topScoreNames[0][2];

    topScores[0] = score;
  }
  else if (score >= topScores[1]){
    index = 1;
    topScores[2] = topScores[1];
    
    topScoreNames[2][0] = topScoreNames[1][0];
    topScoreNames[2][1] = topScoreNames[1][1];
    topScoreNames[2][2] = topScoreNames[1][2];
    
    topScores[1] = score;
  }
  else if (score >= topScores[2]){
    index = 2;
    topScores[2] = score;
  }
  else{
    return;  
  }

  drawString("nice" , 1, 1);

  char selectedLetter = 0;
  char startOffsets[] = {6, 14, 21};
  
  drawChar('a', 7, 10);
  drawChar('a', 15, 10);
  drawChar('a', 22, 10);
  topScoreNames[index][0] = 'a';
  topScoreNames[index][1] = 'a';
  topScoreNames[index][2] = 'a';
  while(selectedLetter < 3){
    getMenuInput();

    if(newInput < 4){
      direction = newInput;
      newInput = 4;

      switch(direction){
        case 0:
          matrix.fillRect(startOffsets[selectedLetter], 10, 5, 5, color_blank);
          --topScoreNames[index][selectedLetter];
          break;
        case 1:
          if(selectedLetter > 0){
            --selectedLetter;
          }
          break;
        case 2:
          ++selectedLetter;
          break;
        case 3:
          matrix.fillRect(startOffsets[selectedLetter], 10, 5, 5, color_blank);
          ++topScoreNames[index][selectedLetter];
          break;
      }

      if (selectedLetter != 3){
        switch(topScoreNames[index][selectedLetter]){
          case 47: topScoreNames[index][selectedLetter] = 122; break;
          case 58: topScoreNames[index][selectedLetter] = 97; break;
          case 96: topScoreNames[index][selectedLetter] = 57; break;
          case 123: topScoreNames[index][selectedLetter] = 48; break;
        }
  
        drawChar(topScoreNames[index][selectedLetter], startOffsets[selectedLetter] + getLetterOffset(topScoreNames[index][selectedLetter]), 10);
        direction = 4;
        lastFrameTime = millis();
        delay(500);
      }
    }
  }
}

void showTopScores(){
  matrix.fillScreen(color_blank);

  char offsetY = 1;
  char i = 0;
  while (i<3 && topScores[i] > 0){
    drawString(String(topScoreNames[i][0]) + String(topScoreNames[i][1]) + String(topScoreNames[i][2]) , 1, offsetY);
    drawString(String(topScores[i]), 20, offsetY);
    offsetY += 5;
    ++i;
  }

  waitForButtonInMenu();
}

void waitForButtonInMenu(){
  newInput = 4;
  delay(500);
  while (newInput == 4){
    getMenuInput();
  }  
  delay(500);
  newInput = 4;
}

void getMenuInput() {
  if(digitalRead(50) == HIGH) {
    newInput = 0;
  }
  else if(digitalRead(51) == HIGH) {
    newInput = 1;
  }
  else if(digitalRead(52) == HIGH) {
    newInput = 2;
  }
  else if(digitalRead(53) == HIGH) {
    newInput = 3;
  }
  return 127;
}

void getDirection() {
  if(digitalRead(50) == HIGH && direction != 3) {
    newInput = 0;
  }
  else if(digitalRead(51) == HIGH && direction != 2) {
    newInput = 1;
  }
  else if(digitalRead(52) == HIGH && direction != 1) {
    newInput = 2;
  }
  else if(digitalRead(53) == HIGH && direction != 0) {
    newInput = 3;
  }
}

void drawString(String tekst, int16_t offsetX, int16_t offsetY){
  for(char i=0; i<tekst.length(); ++i){
    offsetX += drawChar(tekst[i], offsetX, offsetY);  
  }
}

char drawChar(char letter, int16_t x, int16_t y){
  uint8_t* bitmap = charToBitmap(letter);
  char width = getLetterWidth(letter);

  for(char i=0; i<width*4; ++i){
    char drawX = x+(i%width);
    char drawY = y+(char)(i/width + 0.2);
      
    if(bitmap[i] == 1){
      matrix.drawPixel(drawX, drawY, color_green);
    }
    else{
      matrix.drawPixel(drawX, drawY, color_blank);
    }
  }
  return width + 1;
}

uint8_t* charToBitmap(char letter){
  
  switch(letter){
    case 'a': return _a;
    case 'b': return _b;
    case 'c': return _c;
    case 'd': return _d;
    case 'e': return _e;
    case 'f': return _f;
    case 'g': return _g;
    case 'h': return _h;
    case 'i': return _i;
    case 'j': return _j;
    case 'k': return _k;
    case 'l': return _l;
    case 'm': return _m;
    case 'n': return _n;
    case 'o': return _o;
    case 'p': return _p;
    case 'q': return _q;
    case 'r': return _r;
    case 's': return _s;
    case 't': return _t;
    case 'u': return _u;
    case 'v': return _v;
    case 'w': return _w;
    case 'x': return _x;
    case 'y': return _y;
    case 'z': return _z;
    case '0': return _o;
    case '1': return _1;
    case '2': return _2;
    case '3': return _3;
    case '4': return _4;
    case '5': return _s;
    case '6': return _6;
    case '7': return _7;
    case '8': return _8;
    case '9': return _9;
    default: return _empty;
  }
}

char getLetterWidth(char letter){
  switch(letter){
    case 'a': return 3;
    case 'b': return 3;
    case 'c': return 3;
    case 'd': return 3;
    case 'e': return 3;
    case 'f': return 2;
    case 'g': return 3;
    case 'h': return 3;
    case 'i': return 1;
    case 'j': return 2;
    case 'k': return 3;
    case 'l': return 2;
    case 'm': return 5;
    case 'n': return 3;
    case 'o': return 3;
    case 'p': return 3;
    case 'q': return 3;
    case 'r': return 3;
    case 's': return 2;
    case 't': return 3;
    case 'u': return 3;
    case 'v': return 3;
    case 'w': return 5;
    case 'x': return 3;
    case 'y': return 3;
    case 'z': return 2;
    case '0': return 3;
    case '1': return 3;
    case '2': return 3;
    case '3': return 3;
    case '4': return 3;
    case '5': return 2;
    case '6': return 2;
    case '7': return 2;
    case '8': return 3;
    case '9': return 3;
    default: return 0;
  }
}

char getLetterOffset(char letter){
    char width = getLetterWidth(letter);
    switch(width){
      case 1: return 2;
      case 2: return 1;
      case 3: return 1;
      case 5: return 0;
      default: return 1;  
    }
}

void playEffect(int effectnummer){
  Wire.beginTransmission(I2C_EFFECT_ID);
  Wire.write(effectnummer);
  Wire.endTransmission();  
}
