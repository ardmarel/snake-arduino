#include "pitches.h"
#include <Wire.h>

int id = 10;
int tempo =180;

// change this to whichever pin you want to use
int buzzer = 10;

int melody[] = {};

// this calculates the duration of a whole note in ms (60s/tempo)*4 beats
int wholenote = (60000 * 4) / tempo;

int divider = 0, noteDuration = 0, notes = 0;
int invoer;
int data;

void setup() {
  Serial.begin(9600);
  erase(12);
  pinMode(buzzer, OUTPUT);
  Wire.begin(id);
  Wire.onReceive(soundChoice);
}

void loop() {
Serial.println("test");
  switch (data) {  
// eats apple
    case 1:
    Serial.println("test");
    melody[0] = NOTE_D6;
    melody[1] = 8;
    melody[2] = NOTE_G6;
    melody[3] = 8;
    notes = 4;
    musicHandler();
    erase(notes);
    break;

// game over
    case 2:
    melody[0] = NOTE_G5;
    melody[1] = 4;
    melody[2] = NOTE_F5;
    melody[3] = 4;
    melody[4] = NOTE_E5;
    melody[5] = 4;
    melody[6] = NOTE_DS5;
    melody[7] = 4;
    melody[8] = NOTE_DS4;
    melody[9] = 4;
    melody[10] = NOTE_D4;
    melody[11] = 4;
    melody[12] = NOTE_CS4;
    melody[13] = 1;
    notes = 14;
    musicHandler();
    erase(notes);
    break;

// high score
    case 3:
    melody[0] = NOTE_D5;
    melody[1] = 4;
    melody[2] = NOTE_E6;
    melody[3] = 4;
    melody[4] = NOTE_C6;
    melody[5] = 4;
    melody[6] = NOTE_B6;
    melody[7] = 4;
    melody[8] = NOTE_A6;
    melody[9] = 8;
    melody[10] = NOTE_G7;
    melody[11] = 8;
    notes = 12;
    musicHandler();
    erase(notes);
    break;
    
// powering up
  case 4:
    melody[0] = NOTE_B6;
    melody[1] = 16;
    melody[2] = NOTE_C7;
    melody[3] = 16;
    melody[4] = NOTE_E7;
    melody[5] = 16;
    melody[6] = NOTE_G7;
    melody[7] = 16;
    melody[8] = REST;
    melody[9] = 1;
    notes = 10;
    musicHandler();
    erase(notes);
    break;
  }
  
  data = invoer;
  invoer = 0;
}

void soundChoice() {
  invoer = Wire.read();
}

void musicHandler() {
  for (int thisNote = 0; thisNote < notes; thisNote = thisNote + 2) {

    // calculates the duration of each note
    divider = melody[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes
    }

    // we only play the note for 90% of the duration, leaving 10% as a pause
    tone(buzzer, melody[thisNote], noteDuration*0.9);

    // Wait for the specief duration before playing the next note.
    delay(noteDuration);
    
    // stop the waveform generation before the next note.
    noTone(buzzer);
  }
}

void erase(int numberOfNotes) {
  for(int i=0; i < numberOfNotes; i++) {
    melody[i] = NULL;
  }
}
