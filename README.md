# Snake arduino

## Description
Some school project that i thought is fun to share (Code and documentation in dutch)

## Features
- Snake
- Teleportation side screen
- Top scores with 3 letter names (reset after boot)
- Sound effects

## Parts
- Arduino mega
- Arduino uno
- Aquafruit 32*16 led board
- Gyroscope (MCPU-6500)
- some buttons
- some 330 ohm resistors
- PIEZO-ELEMENT

## Installation
- Make a device like explained in the electrical scheme(Dutch).
- Install the Adafruit GFX libary and the rgb matrix panel labary in the Arduino IDE
- The code from the Mega folder will be installed on the arduino mega.
- The code from the SoundDevice folder will be installed on the arduino uno.

## Authors and acknowledgment
- Ard van der Marel
- Dominic slikkerveer
- Ghaylee Simonis
- Gijs Kortlever

## License
GNU General Public License v3.0

## Project status
The project is here for backup and CV reasons. Feel free to have some fun with it.
